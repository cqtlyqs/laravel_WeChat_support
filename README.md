# WeChat_login_for_laravel

#### 介绍
WeChat login component for laravel

#### 集成说明
1. 微信登录

#### 使用说明

1. 引入
```
composer require ktnw/wechat_support
```

2. 发布
```
php artisan vendor:publish --provider="Ktnw\WechatSupport\Providers\WeChatSupportServiceProvider"  
```

3. 修改相关class的namespace
```
php artisan support:wechat
```

4. 配置weChatConfig.php

5. 表迁移(wechat_users表、jsapi_ticket表、access_token表)
```
php artisan migrate --path database/migrations/2022_02_22_153342_create_wechat_user.php
```
```
php artisan migrate --path vendor/ktnw/wechat_support/database/migrations/2022_02_11_145423_create_wx_jsapi_ticket_table.php
```
```
php artisan migrate --path vendor/ktnw/wechat_support/database/migrations/2022_02_11_150318_create_wx_access_token_table.php
```
6. 根据实际业务需求，自行完善和扩展WeChatLoginController.



