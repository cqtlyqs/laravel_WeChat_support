<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class WeChatSupportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'support:wechat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update WeChat class namespace.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'support wechat';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->namespaceUpdate();
        $this->reImportClass();
        $this->info($this->type . ' created successfully.');
    }

    private function namespaceUpdate()
    {
        $classes = [
            ["classPath" => 'Http\Controllers\Api\WeChatLoginController.php', 'namespace' => 'App\Http\Controllers\Api'],
            ["classPath" => 'Validators/WeChatLoginValidator.php', 'namespace' => 'App\Validators'],
        ];
        foreach ($classes as $item) {
            $path = app_path($item['classPath']);
            if (!file_exists($path)) {
                $this->info("namespace update fail: file[$path] not exist.");
                return;
            }
            $content = File::get($path);
            File::put($path, preg_replace("/namespace.*/", "namespace " . $item['namespace'] . ";", $content));
        }
    }

    private function reImportClass()
    {
        $classes = [
            ["classPath" => 'Http\Controllers\Api\WeChatLoginController.php', 'reImportClasses' => [
                ['old' => 'Ktnw\WechatSupport\Validators\WeChatLoginValidator', 'new' => 'App\Validators\WeChatLoginValidator'],
            ]],
        ];
        foreach ($classes as $item) {
            $path = app_path($item['classPath']);
            if (!file_exists($path)) {
                $this->info("reImport class update fail: file[$path] not exist.");
                return;
            }
            $reImportClasses = $item["reImportClasses"];
            if (!empty($reImportClasses)) {
                $content = File::get($path);
                foreach ($reImportClasses as $importClass) {
                    $content = str_replace($importClass["old"], $importClass["new"], $content);
                }
                File::put($path, $content);
            }
        }
    }


}
