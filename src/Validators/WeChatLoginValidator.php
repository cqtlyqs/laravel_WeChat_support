<?php

namespace Ktnw\WechatSupport\Validators;

use Prettus\Validator\LaravelValidator;

/**
 * Class WeChatLoginValidator.
 */
class WeChatLoginValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        "authorize"       => [
            "loginRedirect" => "required|starts_with:http"
        ],
        "weChatLogin"     => [
            "code" => 'required'
        ],
        "fetchWxJSConfig" => [
            "shareUrl" => 'required|starts_with:http',
        ],
    ];

}
