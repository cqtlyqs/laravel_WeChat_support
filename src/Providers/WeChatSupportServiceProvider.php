<?php

namespace Ktnw\WechatSupport\Providers;

use Illuminate\Support\ServiceProvider;

class WeChatSupportServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes($this->getPublishFiles());

        // $this->mergeConfigFrom(__DIR__ . '/../../resources/config/smsConfig.php', 'smsConfig')
    }

    private function getPublishFiles(): array
    {
        $data       = $this->publishData();
        $srcData    = array_column($data, "src");
        $targetData = array_column($data, "target");
        $publishes  = [];
        for ($i = 0; $i < count($srcData); $i++) {
            $publishes[$srcData[$i]] = $targetData[$i];
        }
        return $publishes;
    }

    private function publishData(): array
    {
        return [
            ['src' => __DIR__ . '/../../resources/config/weChatConfig.php', 'target' => config_path('weChatConfig.php')],
            ['src' => __DIR__ . '/../Controllers/WeChatLoginController.php', 'target' => app_path("Http/Controllers/Api/WeChatLoginController.php")],
            ['src' => __DIR__ . '/../Validators/WeChatLoginValidator.php', 'target' => app_path("Validators/WeChatLoginValidator.php")],
            ['src' => __DIR__ . '/../Console/Commands/WeChatSupportCommand.php', 'target' => app_path("Console/Commands/WeChatSupportCommand.php")],
            ['src' => __DIR__ . '/../../database/migrations/2022_02_22_153342_create_wechat_user.php', 'target' => database_path("migrations/2022_02_22_153342_create_wechat_user.php")],
            // ['src' => __DIR__ . '/../../database/migrations/2022_02_11_145423_create_wx_jsapi_ticket_table.php', 'target' => database_path("migrations/2022_02_11_145423_create_wx_jsapi_ticket_table.php")],
            // ['src' => __DIR__ . '/../../database/migrations/2022_02_11_150318_create_wx_access_token_table.php', 'target' => database_path("migrations/2022_02_11_150318_create_wx_access_token_table.php")],
        ];
    }


}