<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWechatUser extends Migration
{

    /**
     * @var mixed
     */
    private $tableName;

    /**
     *
     */
    public function __construct()
    {
        $this->tableName = config("weChatConfig.wechat_user_table_name");
    }

    /**
     * Run the migrations.
     * 除标注不可更改字段外，其他字段根据实际业务自行扩展
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->charset   = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->engine    = 'InnoDB';
            $table->bigIncrements('id')->nullable(false)->comment('主键,字段不可更改');
            $table->string('wx_key', 80)->nullable(false)->comment('微信标识,字段不可更改');
            $table->string('openid', 80)->nullable(false)->comment('openid,字段不可更改');
            $table->string('unionid', 125)->nullable(true)->comment('unionid,字段不可更改');
            $table->string('nickname', 215)->nullable(true)->comment('昵称');
            $table->string('head_img', 215)->nullable(true)->comment('头像');
            $table->dateTime('create_time')->nullable(true)->comment('创建时间');
            $table->dateTime('update_time')->nullable(true)->comment('更新时间');
            $table->bigInteger('user_id')->nullable(true)->comment('关联的用户id');
            $table->unique(["wx_key", "openid"], "unique_wxkey_openid");
            $table->index("openid", "idx_openid");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }

}
