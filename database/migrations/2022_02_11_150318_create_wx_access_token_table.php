<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWxAccessTokenTable extends Migration
{
    /**
     * @var mixed
     */
    private $tableName;

    /**
     * 构造函数
     */
    public function __construct()
    {
        $this->tableName = config("weChatConfig.access_token_table_name");
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->charset   = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->engine    = 'InnoDB';
            $table->bigIncrements('id')->nullable(false)->comment('主键,字段不可更改');
            $table->string('wx_key', 20)->nullable(false)->comment('微信key,字段不可更改');
            $table->string('access_token', 255)->nullable(false)->comment('access_token,字段不可更改');
            $table->dateTime('create_time')->nullable(false)->comment('生成时间,字段不可更改');
            $table->dateTime('expires_time')->nullable(false)->comment('到期时间,字段不可更改');
            $table->index(['wx_key'], "idx_wxkey");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
