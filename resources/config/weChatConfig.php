<?php
return [
    "open_domain_name"        => "https://open.weixin.qq.com",                        // 微信授权域名
    "api_domain_name"         => "https://api.weixin.qq.com",                         // 微信接口域名
    "wx_key"                  => "xx",                                                // 微信公众号标识(用于获取微信普通的access_token，最长20位，不可重复)
    "app_id"                  => "xx",                                                // 微信appid
    "app_secret"              => "xx",                                                // 微信app_secret
    "auth_scope"              => "snsapi_userinfo",                                   // 应用授权作用域[snsapi_base, snsapi_userinfo], 默认为snsapi_userinfo
    "auth_redirect_route"     => "xxx",                                               // 用户授权后，重定向的路由，不包含域名。可根据实际需求设置
    "wechat_user_table_name"  => "wechat_users",                                      // 微信用户信息的表
    "jsapi_ticket_table_name" => "wx_jsapi_ticket",                                   // 存放微信jsapi_ticket的表名
    "access_token_table_name" => "wx_access_token",                                   // 存放微信普通access_token的表名
];